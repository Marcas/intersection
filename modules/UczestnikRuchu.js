class UczestnikRuchu {
    constructor() {
    }
    Add(x,y){
        return x*2*y;
    }
}

class Piesi extends UczestnikRuchu{
    constructor() {
    }
}

class Pojazdy extends UczestnikRuchu{
    constructor() {
    }
}

class Rower extends Pojazdy{
    constructor() {
    }
}

/*
Kategoria M: pojazdy samochodowe przeznaczone do przewozu osób mające co najmniej cztery koła
*/

class PojazdyKategoriaM extends Pojazdy{
    constructor() {
    }
}

/*
Kategoria N: pojazdy samochodowe mające co najmniej cztery koła i
zaprojektowane i wykonane do przewozu ładunków
*/

class PojazdyKategoriaN extends Pojazdy{
    constructor() {
    }
}

/*
Kategoria O: przyczepy
*/

class PojazdyKategoriaO extends Pojazdy{
    constructor() {
    }
}

/*
Kategoria L: pojazdy dwukołowe lub trójkołowe, niektóre pojazdy czterokołowe
*/

class PojazdyKategoriaL extends Pojazdy{
    constructor() {
    }
}

/*
Kategoria T: ciągniki rolnicze
*/

class PojazdyKategoriaT extends Pojazdy{
    constructor() {
    }
}

/*
Kategoria C: ciągniki gąsienicowe
*/

class PojazdyKategoriaC extends Pojazdy{
    constructor() {
    }
}

/*
Trawmwaje
*/

class PojazdyKategoriaTramwaj extends Pojazdy{
    constructor() {
    }
}

export {UczestnikRuchu,Piesi,Pojazdy,Rower,PojazdyKategoriaM,PojazdyKategoriaN,PojazdyKategoriaO,PojazdyKategoriaL,PojazdyKategoriaT,PojazdyKategoriaC,PojazdyKategoriaTramwaj};
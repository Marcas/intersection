class Oznakowanie {
    constructor() {
    }
}

class SygnalizacjaSwietlna extends Oznakowanie{
    constructor(){

    }
}

class SygnalizacjaPieszych extends SygnalizacjaSwietlna{
    constructor(){

    }
}

class SygnalizacjaPojazdow extends SygnalizacjaSwietlna{
    constructor(){

    }
}

class Znak extends Oznakowanie{
    constructor(){

    }
}

class Ostrzegawcze extends Znak{
    constructor(){
        
    }
}

class Zakazu extends Znak{
    constructor(){
        
    }
}

class Nakazu extends Znak{
    constructor(){
        
    }
}

class Informacyjne extends Znak{
    constructor(){
        
    }
}

class KierunkuMiejscowosci extends Znak{
    constructor(){
        
    }
}

class Uzupelniajace extends Znak{
    constructor(){
        
    }
}

class DodatkoweSpecjalne extends Znak{
    constructor(){
        
    }
}


export {Oznakowanie,Znak,Ostrzegawcze,Zakazu,Nakazu,Informacyjne,KierunkuMiejscowosci,Uzupelniajace,DodatkoweSpecjalne,SygnalizacjaSwietlna,SygnalizacjaPieszych,SygnalizacjaPojazdow};
class PasRuchu {
    constructor() {
    }
}

class Pobocze extends PasRuchu{
    constructor(){

    }
}

class Przeprawy extends PasRuchu{
    constructor() {
    }
}

class Most extends Przeprawy{
    constructor() {
    }
}

class Wiadukt extends Przeprawy{
    constructor() {
    }
}

class Tunel extends Przeprawy{
    constructor() {
    }
}

class PasRuchuPiesi extends PasRuchu{
    constructor() {
    }
}

class Chodnik extends PasRuchuPiesi{

}

class PasRuchuPojazdy extends PasRuchu{
    constructor() {
    }
}

class TrasaRowerowa extends PasRuchuPojazdy{
    constructor() {
    }
}

class Parking extends PasRuchuPojazdy{
    constructor() {
    }
}

class Zjazd extends PasRuchuPojazdy{
    constructor() {
    }
} 

class Jezdnia extends PasRuchuPojazdy{
    constructor() {
    }
}

class JednoJezdniowa extends Jezdnia{
    constructor()    {

    }
}

class DwuJezdniowa extends Jezdnia{
    constructor()    {

    }
}

export {PasRuchu};